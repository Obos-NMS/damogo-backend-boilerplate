# Backend Boilerplate

Provides the boilerplate to implement a solution for a backend engineer interview assignment

- Install Rust via rustup: https://rustup.rs/

- Install a MariaDB 10.3.11 server locally with no root password and create a database called `interview`

- Create the file, `.env`, and add the line `DATABASE_URL=mysql://root@localhost/interview` to test the database you setup above for tests

- `cargo run` to send GraphQL queries via `0.0.0.0:3000/graphql` or test queries interactively in a web browser with GraphiQL via `0.0.0.0:3000`

- If you want to change `DATABASE_URL`, run `rm -rf target/debug/.fingerprint/damogo-graphql*` before running `cargo run` again

- You can run `RUST_LOG=info cargo run` to see the logs and adjust the log level by changing `RUST_LOG` to `trace`, `debug`, `warn` or `error`

- format code with `cargo fmt`

- lint code with `cargo clippy`

## GraphQL

- Refer to the [Juniper documentation](https://graphql-rust.github.io/juniper/master/) to define GraphQL schema and implement resolvers

## SQLx

- Read [the README](https://github.com/launchbadge/sqlx) to learn how to execute SQL statements in Rust