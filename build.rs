use dotenv::dotenv;
use std::env::var;

fn main() {
    if let Ok(database_url) = var("DATABASE_URL") {
        println!("cargo:rustc-env=DATABASE_URL={}", database_url);
    } else if dotenv().is_ok() {
        println!(
            "cargo:rustc-env=DATABASE_URL={}",
            var("DATABASE_URL").unwrap()
        );
    }
}
